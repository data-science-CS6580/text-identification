from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
import os

print('\033[1mDaniel Child\033[0m')
print("\033[95mPlease Enter path ie: \033[1m./Testing/Austen/Emma.txt \033[0m")

Authors = ['Austen', 'Baum', 'Verne']
dataPath = './Training/'

#thank you to: https://www.geeksforgeeks.org/how-to-iterate-over-files-in-directory-using-python/
def listAllFilesInDirectory(directory=dataPath):
    files = []
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)

        #filter out items in directory that are not files
        if os.path.isfile(f):
            files.append(f)
    
    return files

##DR BALL your approach was way better but I am proud the way I had it set up so I left both
bookPaths = [listAllFilesInDirectory(dataPath + author) for author in Authors]
bookPaths = [d for d in os.listdir(dataPath) if os.path.isdir(os.path.join(dataPath, d))] 
# books = []
# for paths in bookPaths:
#     for path in paths:
#         file = open(path, mode='r')
#         books.append(file.read())
#         file.close()

##Special thanks to Dr ball
corpus = []
labels = []
for directory in bookPaths:
    books = os.listdir(dataPath + directory)
    for book in books:
        labels.append(directory)
        all_words = ""
        with open(dataPath + directory + "/" + book) as f:
            for line in f:
                all_words += line 
        corpus.append(all_words)


vectorizer = TfidfVectorizer(stop_words="english")
MasiveBagOfWords = vectorizer.fit_transform(corpus)



#ML objects set up
naiveBayes = MultinomialNB().fit(MasiveBagOfWords, labels)
svm = SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3, random_state=42, max_iter=50, tol=1e-3).fit(MasiveBagOfWords, labels)
logReg = LogisticRegression(random_state=0, solver='lbfgs').fit(MasiveBagOfWords, labels)
linearsvc = LinearSVC().fit(MasiveBagOfWords, labels)
rfc = RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0).fit(MasiveBagOfWords, labels)

#TESTING
while(True):
    testBook = input("\nWhat text would you like to identify? (Type 'exit' to quit.)")
    if testBook == "exit":
        exit()

    all_words = ''
    with open(testBook, 'r') as f:
        for line in f:
            all_words += line
        tfid_vect_results = vectorizer.transform([all_words])

        predictions = []
        #Run each ML and post their results
        prediction = naiveBayes.predict(tfid_vect_results)
        print(f"Algorithm 1: Naïve Bayes: {prediction[0]}")
        predictions.append(prediction[0])

        prediction = svm.predict(tfid_vect_results)
        print(f"Algorithm 2: SVM: {prediction[0]}")
        predictions.append(prediction[0])

        prediction = logReg.predict(tfid_vect_results)
        print(f"Algorithm 3: Logistic Regression: {prediction[0]}")
        predictions.append(prediction[0])

        prediction = linearsvc.predict(tfid_vect_results)
        print(f"Algorithm 4: LinearSVC: {prediction[0]}")
        predictions.append(prediction[0])

        prediction = rfc.predict(tfid_vect_results)
        print(f"Algorithm 5: Random Forest Classifier: {prediction[0]}")
        predictions.append(prediction[0])


        #generating my final answer
        answerDictionary = {}
        for lab in labels:
            answerDictionary[lab] = 0
        
        for p in predictions:
            answerDictionary[p] += 1
        
        currAuthor = ""
        number=-1
        for key in answerDictionary:
            if answerDictionary[key] > number:
                currAuthor = key
                number = answerDictionary[key]
        print(f'My Answer: {currAuthor}')
        

