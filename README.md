# Text Identification

Read in the four Verne books, the four Austen books, and the four Baum books. Read in an unknown book and decide which author each book belongs to. Algorithms used:

    Naïve Bayes
    SVM (Support Vector Machine)
    Logistic Regression
    LinearSVC
    Random Forest Classifier

## The gist

Read in the four Verne books, the four Austen books, and the four Baum books. Read in an unknown book and decide which author each book belongs to.

I will test your program with a minimum of four different books. I suggest you make it easier on me by having your program prompt for more books or to exit.

For example, after you read in the training set, you will prompt me for the file name of another book. You can presume that that file will be in the same directory as your python code.

After reading in the next book your program will decide which author wrote the book.

All test books will be in plain text (like the training set) and will come directly from the Gutenberg Project. I will only use English versions.

You will use *at least* four algorithms for identifying the given text. The following are required:

    - Naïve Bayes
    - SVM (Support Vector Machine)

You will then use use *at least* two other algorithms when identifying the text.

NOTE: At the end you should have at least 4 distinct algorithms that you use.


## Training Data

- Jules Verne (http://www.gutenberg.org/ebooks/author/60 (Links to an external site.))
- Jane Austen (http://www.gutenberg.org/ebooks/author/68 (Links to an external site.))
- L. Frank Baum (http://www.gutenberg.org/ebooks/author/42 (Links to an external site.))
